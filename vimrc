set nocompatible
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()


set t_Co=256
syntax on
set background=dark
colorscheme molokai


Plugin 'jelera/vim-javascript-syntax'
Plugin 'pangloss/vim-javascript'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'Raimondi/delimitMate'

